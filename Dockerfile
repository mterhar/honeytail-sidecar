# hadolint ignore=DL3007
FROM ghcr.io/k8s-at-home/ubuntu:latest

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
USER root
ARG APP_VERSION

WORKDIR /app

ENV HONEYCOMB_WRITE_KEY NULL
ENV HONEYCOMB_DATASET NULL
ENV HONEYCOMB_SAMPLE_RATE 1
ENV LOG_FILEPATH /logs_to_tail/*
ENV EXTRA_ARGS " "

COPY app /app

RUN apt-get -qq update && \
    apt-get -qq install -y wget ca-certificates && \
    update-ca-certificates && \
    printf "UpdateMethod=docker\nPackageVersion=%s\nPackageAuthor=[Team k8s-at-home](https://github.com/k8s-at-home)" "${APP_VERSION}" > /app/package_info && \
    chmod -R u=rwX,go=rX /app && \
    printf "umask %d" "${UMASK}" >> /etc/bash.bashrc && \
    mv /app/entrypoint.sh /entrypoint.sh && \
    chmod +x /entrypoint.sh && \
    mkdir /logs_to_tail && \
    mkdir /tmp_honeytail && \
    chown kah:kah /tmp_honeytail && \
    echo "getting honetail binary from https://honeycomb.io/download/honeytail/v${APP_VERSION}/honeytail-linux-amd64" && \
    wget -q -O /bin/honeytail https://honeycomb.io/download/honeytail/v${APP_VERSION}/honeytail-linux-amd64 && \
    chmod 755 /bin/honeytail
    
VOLUME [ "/logs_to_tail" ]

USER kah
CMD [ "/bin/sh", "-c", "/app/entrypoint.sh", "$EXTRA_ARGS"]
