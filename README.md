# Honeytail sidecar

This sidecar container is designed to run alongside another application and mount a logs directory to scan. The sidecar runs 2 honeytail processes. One that reads an application's log file, parses, samples, and ships the events to [Honeycomb.io](https://honeycomb.io). The other reads the honeytail output and ship that to the same dataset with a different service name. This allows adding a trigger to detect event shipping disruptions. 

This is provided as a way to get the most out of [Honeycomb's free tier](https://ui.honeycomb.io/signup) which includes 20,000,000 events per month. If applications are configured to send `Info` and higher, it's unlikely to hit that threshold unless there's an error state that needs resolving. 

Container construction: 

* `/app` holds the configuration file to sending the sidecar container's status to honeycomb
* `/entrypoint.sh` is the script that ensures honeycomb is started and restarted occasionally
* `/logs_to_tail` is a directory that can be mounted for honeytail to tail logs
* `/tmp_honeytail` is a where the primary honeytail process writes stdout and stderr so the secondary can get it
* Environment variables to configure honeytail
  * `--writekey=${HONEYCOMB_WRITE_KEY}`
  * `--file=${LOG_FILEPATH}`
  * `--dataset=${HONEYCOMB_DATASET}`
  * `--samplerate=${HONEYCOMB_SAMPLE_RATE}`
  * `--add_field=podname=${POD_NAME}`
  * `--add_field=nodename=${NODE_NAME}`
  * `--add_field=podnamespace=${POD_NAMESPACE}`
  * `--add_field=service.name=${HONEYCOMB_SERVICE_NAME}`
  * `--config=${HONEYCOMB_CONFIG_FILE}`
* Mount a configmap with the application log parser options

For examples of the environment variables, see the yaml spec below. 

# Usage with K8s At Home

[K8s-at-home](https://github.com/k8s-at-home) is a nify helm library that has a bunch of common homelab services deployed in a consistent fashion. It allows sharing things between charts and gitops style management.

Will update after a PR is delivered... so far, I have a fork that has `honeytail` among it's add-ons. Taking a chart values file and adding this will be enough: 

```yaml
addons:
  honeytail: 
    enabled: true
    image:
      repository: registry.gitlab.com/mterhar/honeytail-sidecar
      tag: latest
    env:
      HONEYCOMB_WRITE_KEY: [[REDACTED]]
      HONEYCOMB_DATASET: homelab
      HONEYCOMB_SAMPLE_RATE: "1"
      HONEYCOMB_SERVICE_NAME: something-cool
      LOG_FILEPATH: /config/logs/something-cool.txt
    volumeMounts:
    - name: config
      mountPath: /config
    configFile: |-
      StatusInterval = 30
      [Required Options]
      ParserName = regex

      [Tail Options]
      ReadFrom = end

      [Regex Parser Options]
      LineRegex = (?P<time>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}\.\d+)\W(?P<level>\w+)\W(?P<method>\w+)\W(?P<msg>.*)
      TimeFieldName = time
      TimeFieldFormat = "%Y-%m-%d %H:%M:%S.%N"
```

# Usage without K8s At Home

1. Create a configmap with your configuration in it. If using regex parser or something with a gross command line argument, the yaml and other encoding will mess it up. 

```yaml
apiVersion: v1
data:
  honeytail-config.conf: |
    parser = regex
    backfill = false
    AddFields = "service.name=something-cool"

    [Tail Options]
    ReadFrom = end

    [Regex Parser Options]
    LineRegex = (?P<time>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}\.\d+) (?P<level>\w+) \w+: (?P<protocol>\S+) (?P<method>\S+) (?P<status>\S+) to (?P<client_ip>\S+). Time: (?P<latency_ms>\d+)ms. (?P<url>http.*$)
    TimeFieldName = time
    TimeFieldFormat = "%Y-%m-%d %H:%M:%S.%N"
kind: ConfigMap
metadata:
  name: honeytail-config
  namespace: default
```

If event rates are too high, [sampling can be configured](https://docs.honeycomb.io/getting-data-in/logs/honeytail/#sampling-high-volume-data) in the honeytail configuration configmap. This may be necessary if you tail an ingress controller.

2. Create a honeycomb api key secret so you can refernce it rather than pasting it into the pod spec

3. Add a container like the one below in this example. 

For this to work, the `something-cool` writes logs into it's `/config/logs` directory. They honeytail opens those logs, matches the regex for logs that we want to send and away they go!

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: something-cool
  labels:
    app.kubernetes.io/instance: something-cool
    app.kubernetes.io/name: something-cool
  namespace: default
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: something-cool
  template:
    metadata:
      labels:
        app: something-cool
    spec:
    containers:
      - command: ["/bin/sh","-c"]
        args:
        - /entrypoint.sh
        env:
        - name: HONEYCOMB_WRITE_KEY
          value: [[REDACTED]]
        - name: HONEYCOMB_DATASET
          value: homelab
        - name: HONEYCOMB_SAMPLE_RATE
          value: "1"
        - name: HONEYCOMB_SERVICE_NAME
          value: [[service.name]]
        - name: LOG_FILEPATH
          value: /config/logs/*
        - name: HONEYCOMB_CONFIG_FILE
          value: /honeytail-conf/honeytail-config.conf
        - name: NODE_NAME
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: spec.nodeName
        - name: POD_NAME
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.name
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
        image: registry.gitlab.com/mterhar/honeytail-sidecar:latest
        imagePullPolicy: Always
        name: honeytail
        volumeMounts:
        - mountPath: /config
          name: something-cool
        - mountPath: /honeytail
          name: honeytail-conf
      - name: something-cool
        image: something-cool:latest
        volumeMounts: 
        - mountPath: /something-cool
          name: something-cool
    volumes:
      - name: something-cool
        persistentVolumeClaim:
          claimName: something-cool-pvc
          readOnly: false
      - name: honeytail-conf
        configMap:
          name: honeytail-config
```
