#!/usr/bin/env bash

#shellcheck disable=SC1091
source "/shim/umask.sh"
source "/shim/vpn.sh"

while true
do
honeytail --writekey=${HONEYCOMB_WRITE_KEY} \
    --file=${LOG_FILEPATH} \
    --dataset=${HONEYCOMB_DATASET} \
    --samplerate=${HONEYCOMB_SAMPLE_RATE} \
    --add_field=podname=${POD_NAME} \
    --add_field=nodename=${NODE_NAME} \
    --add_field=podnamespace=${POD_NAMESPACE} \
    --add_field=service.name=${HONEYCOMB_SERVICE_NAME} \
    --config=${HONEYCOMB_CONFIG_FILE} &> /tmp_honeytail/honeytail.log &
HT_PID=$!
sleep 1s
honeytail \
    --writekey=${HONEYCOMB_WRITE_KEY} \
    --dataset=${HONEYCOMB_DATASET} \
    --add_field=podname=${POD_NAME} \
    --add_field=nodename=${NODE_NAME} \
    --add_field=podnamespace=${POD_NAMESPACE} \
    --add_field=service.name=${HONEYCOMB_SERVICE_NAME}-tail \
    --config=/app/honeytail-tail.conf &
HTT_PID=$!
sleep 5m

echo -n "recycling the honeytails by killing $HT_PID and $HTT_PID at "
date
kill $HT_PID
sleep 5s
kill $HTT_PID
done
